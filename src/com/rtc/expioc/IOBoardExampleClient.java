package com.rtc.expioc;

import java.util.ArrayList;
import java.util.Calendar;

import com.rtc.andioslib.Clients.ClientSideHandler;
import com.rtc.andioslib.Common.IOBoardSettings;
import com.rtc.andioslib.Common.MessageTypes;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class IOBoardExampleClient extends Activity {
	public final static String TAG = "IOBoardClient";
	private Context activityContext;
	
	ScrollView debugScrollView;
	TextView debugDisplay;
	
	TextView mfgView;
	TextView modelView;
	TextView firmwareView;	
	
	TextView sampleView;
	TextView modeView;
	TextView resView;
	TextView vrefView;
	TextView conversionView;
	TextView dataformatView;
	TextView thresholdView;
	TextView hysteresisView;
	TextView continuousdelayView;
	TextView continuousdelaySeekView;
	TextView thresholdSeekView;
	TextView hysteresisSeekView;
	SeekBar adcSampleGraph;
	SeekBar thresholdSeekbar;
	SeekBar hysteresisSeekbar;
	SeekBar continuousdelaySeekbar;
	Switch adcModeControl;
	Switch adcResControl;
	Switch adcFormatControl;
	Switch adcVrefControl;
	
	TextView intDPPView;
	TextView extDPPView;
	TextView extPWRView;
	
	TextView baudrateView;
	TextView databitsView;
	TextView parityView;
	TextView stopbitsView;
	TextView flowcontrolView;
	
	TextView spimodeView;
	TextView primaryscalerView;
	TextView secondaryscalerView;
	
	TextView portView;
	TextView portdirView;
	TextView pullupView;
	TextView cnenableView;
	
	Button resetButton;
	Button commitButton;
	
	ManagerThread threadManager;
	IOBoardSettings boardSettings = new IOBoardSettings();
	
	Messenger mService = null;
	boolean isBound = false;
	String myText = new String();
	
	final Messenger mMessenger = new Messenger(new ClientSideHandler(){

		@Override
		protected void MSGHandler_MSG_S_RESPONSE(Bundle data) {
			
			Log.d(TAG, "MSG_S_RESPONSE Received");
			myText += "MSG_S_RESPONSE Received.\n";
			
			switch(data.getInt("initiating MSG type")){
				case MessageTypes.MSG_READ_ADC:		actionPerformed_MSG_READ_ADC(data);
					break;
				case MessageTypes.MSG_SYSINFO_MFG:	actionPerformed_MSG_SYSINFO_MFG(data);
					break;
				case MessageTypes.MSG_SYSINFO_MODEL:actionPerformed_MSG_SYSINFO_MODEL(data);
					break;
				case MessageTypes.MSG_SYSINFO_FWV:	actionPerformed_MSG_SYSINFO_FWV(data);
					break;
				case MessageTypes.MSG_SYSINFO:		actionPerformed_MSG_SYSINFO(data);
					break;
				case MessageTypes.MSG_CFG_G_DPP:	actionPerformed_MSG_CFG_G_DPP(data);
					break;
				case MessageTypes.MSG_CFG_G_ADC:	actionPerformed_MSG_CFG_G_ADC(data);
					break;
				case MessageTypes.MSG_CFG_G_UART:	actionPerformed_MSG_CFG_G_UART(data);
					break;
				case MessageTypes.MSG_CFG_G_SPI:	actionPerformed_MSG_CFG_G_SPI(data);
					break;
				case MessageTypes.MSG_CFG_G_IO:		actionPerformed_MSG_CFG_G_DIO(data);
					break;
				case MessageTypes.MSG_READ_DIO:		actionPerformed_MSG_READ_DIO(data);
					break;
			}
				
			updateTextDisplay();
			threadManager.clearThreadLock();
		}
		
		private void actionPerformed_MSG_READ_ADC(Bundle data){
			int[] samples = data.getIntArray("samples");
			
			String samplesText = new String();
			myText += "Samples:";
			for(int x = 0; x < samples.length; x++){
				myText += "\t" + samples[x];
				samplesText += "\t" + samples[x];
			}
			myText += "\n";
			
			sampleView.setText(samplesText);
			adcSampleGraph.setProgress(samples[0]);
		}
		private void actionPerformed_MSG_SYSINFO_MFG(Bundle data){
			myText += "USB Device Manufacturer: " + data.getString("MFG") + "\n";
			mfgView.setText(data.getString("MFG"));
			
			boardSettings.setMfg(data.getString("MFG"));
		}
		private void actionPerformed_MSG_SYSINFO_MODEL(Bundle data){
			myText += "USB Device Model: " + data.getString("Model") + "\n";
			modelView.setText(data.getString("Model"));
			
			boardSettings.setModel(data.getString("Model"));
		}
		private void actionPerformed_MSG_SYSINFO_FWV(Bundle data){
			myText += "USB Device Firmware Version: " + data.getString("fwversion") + "\n";
			firmwareView.setText(data.getString("fwversion"));
			
			boardSettings.setFirmwareVersion(data.getString("fwversion"));
		}
		private void actionPerformed_MSG_SYSINFO(Bundle data){
			myText += "USB Device Manufacturer: " + data.getString("MFG") + "\n";
			mfgView.setText(data.getString("MFG"));
			
			myText += "USB Device Model: " + data.getString("Model") + "\n";
			modelView.setText(data.getString("Model"));
			
			myText += "USB Device Firmware Version: " + data.getString("fwversion") + "\n";
			firmwareView.setText(data.getString("fwversion"));
			
			boardSettings.setMfg(data.getString("MFG"));
			boardSettings.setModel(data.getString("Model"));
			boardSettings.setFirmwareVersion(data.getString("fwversion"));
		}
		private void actionPerformed_MSG_CFG_G_DPP(Bundle data){
			if(data.getInt("internal dpp mode") == IOBoardSettings.DPP_MODE_OFF)
				intDPPView.setText("OFF");
			else if(data.getInt("internal dpp mode") == IOBoardSettings.DPP_MODE_DIO)
				intDPPView.setText("Digital IO");
			else if(data.getInt("internal dpp mode") == IOBoardSettings.DPP_MODE_UART)
				intDPPView.setText("UART");
			else if(data.getInt("internal dpp mode") == IOBoardSettings.DPP_MODE_SPI)
				intDPPView.setText("SPI");
			
			if(data.getInt("external dpp mode") == IOBoardSettings.DPP_MODE_OFF)
				extDPPView.setText("OFF");
			else if(data.getInt("external dpp mode") == IOBoardSettings.DPP_MODE_DIO)
				extDPPView.setText("Digital IO");
			else if(data.getInt("external dpp mode") == IOBoardSettings.DPP_MODE_UART)
				extDPPView.setText("UART");
			else if(data.getInt("external dpp mode") == IOBoardSettings.DPP_MODE_SPI)
				extDPPView.setText("SPI");
			
			if(data.getInt("external power mode") == IOBoardSettings.DPP_EXTPWR_OFF)
				extPWRView.setText("OFF");
			else if(data.getInt("external power mode") == IOBoardSettings.DPP_EXTPWR_3V3)
				extPWRView.setText("3.3V Output Enabled");
			else if(data.getInt("external power mode") == IOBoardSettings.DPP_EXTPWR_5V0)
				extPWRView.setText("5.0V Output Enabled");
		}
		private void actionPerformed_MSG_CFG_G_ADC(Bundle data){
			
			if(data.getInt("mode") == IOBoardSettings.ADC_MODE_DISCREET){
				modeView.setText("Discreet");
				adcModeControl.setChecked(false);
			}
			else if(data.getInt("mode") == IOBoardSettings.ADC_MODE_CONTINUOUS){
				modeView.setText("Continuous");
				adcModeControl.setChecked(true);
			}
			
			if(data.getInt("resolution") == IOBoardSettings.ADC_RESOLUTION_8BIT){
				resView.setText("8-bit");
				adcResControl.setChecked(false);
				thresholdSeekbar.setMax(255);
			}
			else if(data.getInt("resolution") == IOBoardSettings.ADC_RESOLUTION_10BIT){
				resView.setText("10-bit");
				adcResControl.setChecked(true);
				thresholdSeekbar.setMax(65535);
			}
			
			if(data.getInt("voltage ref") == IOBoardSettings.ADC_VREF_3V3){
				vrefView.setText("3.3V");
				adcVrefControl.setChecked(false);
			}
			else if(data.getInt("voltage ref") == IOBoardSettings.ADC_VREF_EXT){
				vrefView.setText("External Vref");
				adcVrefControl.setChecked(true);
			}

			conversionView.setText(new String(data.getInt("conversion delay") + " ms"));
			
			if(data.getInt("format") == IOBoardSettings.ADC_JUSTIFIED_LEFT){
				dataformatView.setText("Left Justified");
				adcFormatControl.setChecked(false);
			}
			else if(data.getInt("format") == IOBoardSettings.ADC_JUSTIFIED_RIGHT){
				dataformatView.setText("Right Justified");
				adcFormatControl.setChecked(true);
			}
			
			thresholdView.setText(String.valueOf(data.getInt("threshold")));
			thresholdSeekbar.setProgress(data.getInt("threshold"));
			
			hysteresisView.setText(String.valueOf(data.getInt("hysteresis")));
			hysteresisSeekbar.setProgress(data.getInt("hysteresis"));
			
			double contDelay = data.getInt("continuous delay");
			contDelay = contDelay/10.0;
			continuousdelayView.setText(String.valueOf(contDelay) + " seconds");
			continuousdelaySeekbar.setProgress((int)(contDelay*10));
			
			//Store all the settings for use later when trying to set just 1 parameter
			boardSettings.setAdcMode(data.getInt("mode"));
			boardSettings.setAdcResolution(data.getInt("resolution"));
			boardSettings.setAdcRefVoltage(data.getInt("voltage ref"));
			boardSettings.setAdcConversionDelay(data.getInt("conversion delay"));
			boardSettings.setAdcDataFormat(data.getInt("format"));
			boardSettings.setAdcThreshold(data.getInt("threshold"));
			boardSettings.setAdcHysteresis(data.getInt("hysteresis"));
			boardSettings.setAdcContinuousDelay(data.getInt("continuous delay"));
			
			updateADCSampleGraphMax();
			
			unlockChangeControls();
			
		}
		private void actionPerformed_MSG_CFG_G_UART(Bundle data){
			switch(data.getInt("baudrate")){
				case IOBoardSettings.UART_BAUD_9600:	baudrateView.setText("9600 bps");
					break;
				case IOBoardSettings.UART_BAUD_19200:	baudrateView.setText("19200 bps");
					break;
				case IOBoardSettings.UART_BAUD_38400:	baudrateView.setText("38400 bps");
					break;
				case IOBoardSettings.UART_BAUD_57600:	baudrateView.setText("57600 bps");
					break;
				case IOBoardSettings.UART_BAUD_115200:	baudrateView.setText("115200 bps");
					break;
				default:	baudrateView.setText(String.valueOf(data.getInt("baudrate")));
			}
			
			databitsView.setText("8");
			
			if(data.getInt("parity") == IOBoardSettings.UART_PARITY_NONE)
				parityView.setText("None");
			else if(data.getInt("parity") == IOBoardSettings.UART_PARITY_EVEN)
				parityView.setText("Even");
			else if(data.getInt("parity") == IOBoardSettings.UART_PARITY_ODD)
				parityView.setText("odd");
			
			if(data.getInt("stopbits") == IOBoardSettings.UART_STOPBITS_ONE)
				stopbitsView.setText("1");
			else if(data.getInt("stopbits") == IOBoardSettings.UART_STOPBITS_TWO)
				stopbitsView.setText("2");
			
			if(data.getInt("flowcntl") == IOBoardSettings.UART_FLOWCNTL_NONE)
				flowcontrolView.setText("None");
			else if(data.getInt("flowcntl") == IOBoardSettings.UART_FLOWCNTL_RTSCTS)
				flowcontrolView.setText("RTS/CTS");
		}
		private void actionPerformed_MSG_CFG_G_SPI(Bundle data){
			spimodeView.setText(String.valueOf(data.getInt("spimode")));
			primaryscalerView.setText(String.valueOf(data.getInt("clock primary prescaler")));
			secondaryscalerView.setText(String.valueOf(data.getInt("clock secondary prescaler")));
		}
		
		private void actionPerformed_MSG_CFG_G_DIO(Bundle data){
			
			String portdir_str = java.lang.Integer.toString(0x20 | data.getInt("Port dir"), 2).substring(1);
			String pullup_str = java.lang.Integer.toString(0x20 | data.getInt("PU Enables"), 2).substring(1);
			String cnenabler_str = java.lang.Integer.toString(0x20 | data.getInt("CN Enables"), 2).substring(1);
			
			portdirView.setText(portdir_str);
			pullupView.setText(pullup_str);
			cnenableView.setText(cnenabler_str);
		}
		
		private void actionPerformed_MSG_READ_DIO(Bundle data){
			portView.setText(java.lang.Integer.toString(0x20 | data.getInt("data"), 2).substring(1));
		}

		@Override
		protected void MSGHandler_MSG_U_RESPONSE(Bundle data) {
			Log.d(TAG, "MSG_U_RESPONSE Received");
			myText += "MSG_U_RESPONSE Received.\n";
			
			if(data.getInt("peripheral") == IOBoardSettings.IOBOARD_PERIPHERAL_ADC){
				int sample = data.getInt("data");
				
				String samplesText = "\t" + sample;
				
				sampleView.setText(samplesText);
				adcSampleGraph.setProgress(sample);
			}
			
			updateTextDisplay();
		}

		@Override
		protected void MSGHandler_MSG_ACK(Bundle data) {
			Log.d(TAG, "MSG_ACK Received");
			myText += "MSG_ACK Received.\n";
			
			if(data.getInt("initiating MSG type") == MessageTypes.MSG_ADMIN_COMMIT)
				Toast.makeText(activityContext, "Settings Committed Successfully", Toast.LENGTH_SHORT).show();
			else if(data.getInt("initiating MSG type") == MessageTypes.MSG_ADMIN_RESET)
				Toast.makeText(activityContext, "Settings Reset Successfully", Toast.LENGTH_SHORT).show();
			else if(data.getInt("initiating MSG type") == MessageTypes.MSG_REGISTER_PERIPHERAL)
				Toast.makeText(activityContext, "Registered with Service as ADC Client", Toast.LENGTH_SHORT).show();
			
			updateTextDisplay();
			threadManager.clearThreadLock();
		}

		@Override
		protected void MSGHandler_MSG_ERR(Bundle data) {
			Log.d(TAG, "MSG_ERR Received");
			myText += "************************* MSG_ERR Received. *************************\n";
			myText += data.getString("error message") + "\n";
			myText += "*********************************************************************\n";
			
			updateTextDisplay();
			threadManager.clearThreadLock();
			unlockChangeControls();
		}
		
	});
	
	/**
	 * Class for interacting with the interface of the service.
	 */
	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
            mService = new Messenger(service);
            myText += "Connected to service.\n";
            updateTextDisplay();
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mService = null;
            myText += "Disconnected from service.\n";
            updateTextDisplay();
        }
    };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ioboard_example_client);
		
		activityContext = this;
		
		mfgView = (TextView) this.findViewById(R.id.mfgViewField);
		modelView = (TextView) this.findViewById(R.id.modelViewField);
		firmwareView = (TextView) this.findViewById(R.id.firmwareViewField);
		
		sampleView = (TextView) this.findViewById(R.id.sampleViewField);
		modeView = (TextView) this.findViewById(R.id.modeViewField);
		resView = (TextView) this.findViewById(R.id.resViewField);
		vrefView = (TextView) this.findViewById(R.id.vrefViewField);
		conversionView = (TextView) this.findViewById(R.id.conversionViewField);
		dataformatView = (TextView) this.findViewById(R.id.dataformatViewField);
		thresholdView = (TextView) this.findViewById(R.id.thresholdViewField);
		hysteresisView = (TextView) this.findViewById(R.id.hysteresisViewField);
		continuousdelayView = (TextView) this.findViewById(R.id.continuousdelayViewField);
		continuousdelaySeekView = (TextView) this.findViewById(R.id.continuousdelaySeekField);
		thresholdSeekView = (TextView) this.findViewById(R.id.thresholdSeekField);
		hysteresisSeekView = (TextView) this.findViewById(R.id.hysteresisSeekField);
		adcSampleGraph = (SeekBar) this.findViewById(R.id.sampleScaleBar);
		adcModeControl = (Switch) this.findViewById(R.id.adcModeControl);
		adcResControl = (Switch) this.findViewById(R.id.resControl);
		adcFormatControl = (Switch) this.findViewById(R.id.adcFormatControl);
		adcVrefControl = (Switch) this.findViewById(R.id.adcVrefControl);
		thresholdSeekbar = (SeekBar) this.findViewById(R.id.thresholdSeekbar);
		hysteresisSeekbar = (SeekBar) this.findViewById(R.id.hysteresisSeekbar);
		continuousdelaySeekbar = (SeekBar) this.findViewById(R.id.continuousdelaySeekbar);
		
		intDPPView = (TextView) this.findViewById(R.id.intdppViewField);
		extDPPView = (TextView) this.findViewById(R.id.extdppViewField);
		extPWRView = (TextView) this.findViewById(R.id.extpwrViewField);
		
		baudrateView = (TextView) this.findViewById(R.id.baudrateViewField);
		databitsView = (TextView) this.findViewById(R.id.databitsViewField);
		parityView = (TextView) this.findViewById(R.id.parityViewField);
		stopbitsView = (TextView) this.findViewById(R.id.stopbitsViewField);
		flowcontrolView = (TextView) this.findViewById(R.id.flowcontrolViewField);
		
		spimodeView = (TextView) this.findViewById(R.id.spimodeViewField);
		primaryscalerView = (TextView) this.findViewById(R.id.primaryscalerViewField);
		secondaryscalerView = (TextView) this.findViewById(R.id.secondaryscalerViewField);
		
		portView = (TextView) this.findViewById(R.id.portViewField);
		portdirView = (TextView) this.findViewById(R.id.portdirViewField);
		pullupView = (TextView) this.findViewById(R.id.pullupViewField);
		cnenableView = (TextView) this.findViewById(R.id.cnenableViewField);
		
		resetButton = (Button) this.findViewById(R.id.resetButton);
		commitButton = (Button) this.findViewById(R.id.commitButton);
		
		debugDisplay = (TextView) this.findViewById(R.id.debugDisplay);
		debugScrollView = (ScrollView) this.findViewById(R.id.debugScrollView);
		
		adcSampleGraph.setOnTouchListener(new OnTouchListener(){
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				//always returning true disables the touch feature
				return true;
			}
	    });
		
		thresholdSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				if(thresholdSeekView.isEnabled()){
					thresholdSeekView.setText(String.valueOf(thresholdSeekbar.getProgress()));
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				thresholdSeekView.setEnabled(true);
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				thresholdSeekView.setText("");
				thresholdSeekView.setEnabled(false);
				thresholdSeekbar_onChangedActionPerformed();
			}
			
		});
		
		hysteresisSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				if(hysteresisSeekView.isEnabled()){
					hysteresisSeekView.setText(String.valueOf(hysteresisSeekbar.getProgress()));
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				hysteresisSeekView.setEnabled(true);
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				hysteresisSeekView.setText("");
				hysteresisSeekView.setEnabled(false);
				hysteresisSeekbar_onChangedActionPerformed();
			}
			
		});
		
		continuousdelaySeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener(){

			@Override
			public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
				if(continuousdelaySeekView.isEnabled()){
					double value = continuousdelaySeekbar.getProgress() / 10.0;
					continuousdelaySeekView.setText(String.valueOf(value));
				}
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				continuousdelaySeekView.setEnabled(true);
			}

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				continuousdelaySeekView.setText("");
				continuousdelaySeekView.setEnabled(false);
				continuousdelaySeekbar_onChangedActionPerformed();
			}
			
		});
		
		
		adcModeControl.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
				//check if the initial value has been set yet
				if(boardSettings.getAdcMode() != -1)
					adcChangeModeRequest();
			}
		});
		
		adcResControl.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
				//check if the initial value has been set yet
				if(boardSettings.getAdcResolution() != -1)
					adcChangeResolutionRequest();
			}
		});
		
		adcFormatControl.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
				//check if the initial value has been set yet
				if(boardSettings.getAdcDataFormat() != -1)
					adcChangeDataFormatRequest();
			}
		});
		
		adcVrefControl.setOnCheckedChangeListener(new OnCheckedChangeListener(){

			@Override
			public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
				//check if the initial value has been set yet
				if(boardSettings.getAdcRefVoltage() != -1)
					adcChangeVrefRequest();
			}
		});
		
		resetButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				//only perform if connection to service is complete
				if(mService != null){
					threadManager.queue(new resetSettingsThread());
					
					//clear these adc settings so that the change listeners do not trigger
					boardSettings.setAdcMode(-1);
					boardSettings.setAdcResolution(-1);
					boardSettings.setAdcDataFormat(-1);
					boardSettings.setAdcRefVoltage(-1);
					
					threadManager.queue(new getADCSetupThread());
				}
			}
		});
		
		commitButton.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				//only perform if connection to service is complete
				if(mService != null)
					threadManager.queue(new commitSettingsThread());
			}
		});
		
		doBindService();
		
		threadManager = new ManagerThread();
		threadManager.start();
		
		threadManager.queue(new getSysAllInfoThread());
		threadManager.queue(new getADCSetupThread());
		threadManager.queue(new getDPPSetupThread());
		threadManager.queue(new getUARTSetupThread());
		threadManager.queue(new getSPISetupThread());
		threadManager.queue(new getDIOSetupThread());
		threadManager.queue(new readDIOThread());
		
		threadManager.queue(new registerADCClientThread());
				
	}
	
	private void continuousdelaySeekbar_onChangedActionPerformed() {
		lockChangeControls();
		
		//create and send a message
		Bundle b = new Bundle();
		
		//Use the saved settings that we don't want to change
		b.putInt("mode", boardSettings.getAdcMode());
		b.putInt("resolution", boardSettings.getAdcResolution());
		b.putInt("threshold", boardSettings.getAdcThreshold());
		b.putInt("hysteresis", boardSettings.getAdcHysteresis());
		b.putInt("voltage ref", boardSettings.getAdcRefVoltage());
		b.putInt("conversion delay", boardSettings.getAdcConversionDelay());
		b.putInt("format", boardSettings.getAdcDataFormat());
		
		
		//set the settings that we want to change based on the UI input
		int delayValue = continuousdelaySeekbar.getProgress();
		b.putInt("continuous delay", delayValue);
		
		//schedule up the thread to send a change settings message
    	threadManager.queue(new setADCSetupThread(b));
    	//schedule up the thread to send a get settings message
    	threadManager.queue(new getADCSetupThread());
	}
	
	private void thresholdSeekbar_onChangedActionPerformed() {
		lockChangeControls();
		
		//create and send a message
		Bundle b = new Bundle();
		
		//Use the saved settings that we don't want to change
		b.putInt("mode", boardSettings.getAdcMode());
		b.putInt("resolution", boardSettings.getAdcResolution());
		b.putInt("continuous delay", boardSettings.getAdcContinuousDelay());
		b.putInt("hysteresis", boardSettings.getAdcHysteresis());
		b.putInt("voltage ref", boardSettings.getAdcRefVoltage());
		b.putInt("conversion delay", boardSettings.getAdcConversionDelay());
		b.putInt("format", boardSettings.getAdcDataFormat());
		
		
		//set the settings that we want to change based on the UI input
		int delayValue = thresholdSeekbar.getProgress();
		b.putInt("threshold", delayValue);
		
		//schedule up the thread to send a change settings message
    	threadManager.queue(new setADCSetupThread(b));
    	//schedule up the thread to send a get settings message
    	threadManager.queue(new getADCSetupThread());
	}
	
	private void hysteresisSeekbar_onChangedActionPerformed() {
		lockChangeControls();
		
		//create and send a message
		Bundle b = new Bundle();
		
		//Use the saved settings that we don't want to change
		b.putInt("mode", boardSettings.getAdcMode());
		b.putInt("resolution", boardSettings.getAdcResolution());
		b.putInt("threshold", boardSettings.getAdcThreshold());
		b.putInt("continuous delay", boardSettings.getAdcContinuousDelay());
		b.putInt("voltage ref", boardSettings.getAdcRefVoltage());
		b.putInt("conversion delay", boardSettings.getAdcConversionDelay());
		b.putInt("format", boardSettings.getAdcDataFormat());
		
		
		//set the settings that we want to change based on the UI input
		int delayValue = hysteresisSeekbar.getProgress();
		b.putInt("hysteresis", delayValue);
		
		//schedule up the thread to send a change settings message
    	threadManager.queue(new setADCSetupThread(b));
    	//schedule up the thread to send a get settings message
    	threadManager.queue(new getADCSetupThread());
	}
	
	private void updateADCSampleGraphMax(){
		int max = 0;
		
		if(boardSettings.getAdcResolution() == IOBoardSettings.ADC_RESOLUTION_8BIT && boardSettings.getAdcDataFormat() == IOBoardSettings.ADC_JUSTIFIED_LEFT)
			max = 255;
		else if(boardSettings.getAdcResolution() == IOBoardSettings.ADC_RESOLUTION_8BIT && boardSettings.getAdcDataFormat() == IOBoardSettings.ADC_JUSTIFIED_RIGHT)
			max = 255;
		else if(boardSettings.getAdcResolution() == IOBoardSettings.ADC_RESOLUTION_10BIT && boardSettings.getAdcDataFormat() == IOBoardSettings.ADC_JUSTIFIED_LEFT)
			max = 65472;
		else if(boardSettings.getAdcResolution() == IOBoardSettings.ADC_RESOLUTION_10BIT && boardSettings.getAdcDataFormat() == IOBoardSettings.ADC_JUSTIFIED_RIGHT)
			max = 1023;
		else
			max = 65535;
		
		adcSampleGraph.setMax(max);
	}
	
	private void lockChangeControls(){
		adcModeControl.setEnabled(false);
		adcResControl.setEnabled(false);
		adcFormatControl.setEnabled(false);
		adcVrefControl.setEnabled(false);
		thresholdSeekbar.setEnabled(false);
		hysteresisSeekbar.setEnabled(false);
		continuousdelaySeekbar.setEnabled(false);
	}
	
	private void unlockChangeControls(){
		adcModeControl.setEnabled(true);
		adcResControl.setEnabled(true);
		adcFormatControl.setEnabled(true);
		adcVrefControl.setEnabled(true);
		thresholdSeekbar.setEnabled(true);
		hysteresisSeekbar.setEnabled(true);
		continuousdelaySeekbar.setEnabled(true);
	}
	
	private void adcChangeModeRequest(){
		
		lockChangeControls();
		
		//create and send a message
		Bundle b = new Bundle();
		
		//Use the saved settings that we don't want to change
		b.putInt("resolution", boardSettings.getAdcResolution());
		b.putInt("threshold", boardSettings.getAdcThreshold());
		b.putInt("continuous delay", boardSettings.getAdcContinuousDelay());
		b.putInt("hysteresis", boardSettings.getAdcHysteresis());
		b.putInt("voltage ref", boardSettings.getAdcRefVoltage());
		b.putInt("conversion delay", boardSettings.getAdcConversionDelay());
		b.putInt("format", boardSettings.getAdcDataFormat());
		
		//set the settings that we want to change based on the UI input
		if(adcModeControl.isChecked())
			b.putInt("mode", IOBoardSettings.ADC_MODE_CONTINUOUS);
		else
			b.putInt("mode", IOBoardSettings.ADC_MODE_DISCREET);
		
		//schedule up the thread to send a change settings message
    	threadManager.queue(new setADCSetupThread(b));
    	//schedule up the thread to send a get settings message
    	threadManager.queue(new getADCSetupThread());

	}
	
	private void adcChangeResolutionRequest(){
		
		lockChangeControls();
		
		//create and send a message
		Bundle b = new Bundle();
		
		//Use the saved settings that we don't want to change
		b.putInt("mode", boardSettings.getAdcMode());
		b.putInt("threshold", boardSettings.getAdcThreshold());
		b.putInt("continuous delay", boardSettings.getAdcContinuousDelay());
		b.putInt("hysteresis", boardSettings.getAdcHysteresis());
		b.putInt("voltage ref", boardSettings.getAdcRefVoltage());
		b.putInt("conversion delay", boardSettings.getAdcConversionDelay());
		b.putInt("format", boardSettings.getAdcDataFormat());
		
		//set the settings that we want to change based on the UI input
		if(adcResControl.isChecked())
			b.putInt("resolution", IOBoardSettings.ADC_RESOLUTION_10BIT);
		else
			b.putInt("resolution", IOBoardSettings.ADC_RESOLUTION_8BIT);
        
		//schedule up the thread to send a change settings message
    	threadManager.queue(new setADCSetupThread(b));
    	//schedule up the thread to send a get settings message
    	threadManager.queue(new getADCSetupThread());

	}
	
	private void adcChangeDataFormatRequest(){
		
		lockChangeControls();
		
		//create and send a message
		Bundle b = new Bundle();
		
		//Use the saved settings that we don't want to change
		b.putInt("mode", boardSettings.getAdcMode());
		b.putInt("resolution", boardSettings.getAdcResolution());
		b.putInt("threshold", boardSettings.getAdcThreshold());
		b.putInt("continuous delay", boardSettings.getAdcContinuousDelay());
		b.putInt("hysteresis", boardSettings.getAdcHysteresis());
		b.putInt("voltage ref", boardSettings.getAdcRefVoltage());
		b.putInt("conversion delay", boardSettings.getAdcConversionDelay());
		
		//set the settings that we want to change based on the UI input
		if(adcFormatControl.isChecked())
			b.putInt("format", IOBoardSettings.ADC_JUSTIFIED_RIGHT);
		else
			b.putInt("format", IOBoardSettings.ADC_JUSTIFIED_LEFT);
        
		//schedule up the thread to send a change settings message
    	threadManager.queue(new setADCSetupThread(b));
    	//schedule up the thread to send a get settings message
    	threadManager.queue(new getADCSetupThread());

	}
	
	private void adcChangeVrefRequest(){
		
		lockChangeControls();
		
		//create and send a message
		Bundle b = new Bundle();
		
		//Use the saved settings that we don't want to change
		b.putInt("mode", boardSettings.getAdcMode());
		b.putInt("resolution", boardSettings.getAdcResolution());
		b.putInt("threshold", boardSettings.getAdcThreshold());
		b.putInt("continuous delay", boardSettings.getAdcContinuousDelay());
		b.putInt("hysteresis", boardSettings.getAdcHysteresis());
		b.putInt("format", boardSettings.getAdcDataFormat());
		b.putInt("conversion delay", boardSettings.getAdcConversionDelay());
		
		//set the settings that we want to change based on the UI input
		if(adcVrefControl.isChecked())
			b.putInt("voltage ref", IOBoardSettings.ADC_VREF_EXT);
		else
			b.putInt("voltage ref", IOBoardSettings.ADC_VREF_3V3);
        
		//schedule up the thread to send a change settings message
    	threadManager.queue(new setADCSetupThread(b));
    	//schedule up the thread to send a get settings message
    	threadManager.queue(new getADCSetupThread());
	}
	
	private void updateTextDisplay(){
		debugDisplay.setText(myText);
		
		if(myText.length() > 1000){
        	myText = myText.substring(myText.length()-800);
        }
		
		debugScrollView.scrollTo(0, debugDisplay.getBottom());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ioboard_example_client, menu);
		return true;
	}
	
	private void doBindService() {
	    try {
	        Intent serviceIntent = new Intent();
	        serviceIntent.setComponent(new ComponentName("com.rtc.ioboardservice", "com.rtc.ioboardservice.IOBoardService"));

	        if (bindService(serviceIntent, mConnection, 0)){
	            myText += "Successfully bound to service\n";
	            isBound = true;
	        } else {
	        	myText += "Failed to bind to service\n";
	        }
	    } catch (SecurityException e) {
	    	myText += "Security Exception: can't bind to IOBoardService, check permission in Manifest";
	    }
	    
	    updateTextDisplay();
    }
	
    private void doUnbindService() {
        if (isBound) {
            // Detach our existing connection.
            unbindService(mConnection);
            isBound = false;
        }
    }

    @Override
    protected void onDestroy() {
    	doUnbindService();
        super.onDestroy();
    }
    
    @Override
    protected void onStop(){
    	//doUnbindService();
    	//super.onStop();
    	
    	onDestroy();
    }
    
 		
	private class getADCSampleThread implements Runnable{
		
		@Override
		public void run() {
			//create and send a message
			Bundle b = new Bundle();
			b.putInt("sample qty", 1);
			Message msg = new Message();
			msg.what = MessageTypes.MSG_READ_ADC;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_READ_ADC Message");
	        	myText += "Sending MSG_READ_ADC Message\n";       	
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private class getSysAllInfoThread implements Runnable{
		
		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			Bundle b = new Bundle();
			msg.what = MessageTypes.MSG_SYSINFO;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_SYSINFO Message");
	        	myText += "Sending MSG_SYSINFO Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
        
	}
	
	private class getADCSetupThread implements Runnable{
		
		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			Bundle b = new Bundle();
			msg.what = MessageTypes.MSG_CFG_G_ADC;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_CFG_G_ADC Message");
	        	myText += "Sending MSG_CFG_G_ADC Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private class setADCSetupThread implements Runnable{
		private Bundle b;
		
		setADCSetupThread(Bundle b){
			this.b = b;
		}
		
		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			msg.what = MessageTypes.MSG_CFG_S_ADC;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_CFG_S_ADC Message");
	        	myText += "Sending MSG_CFG_S_ADC Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private class getDPPSetupThread implements Runnable{
		
		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			Bundle b = new Bundle();
			msg.what = MessageTypes.MSG_CFG_G_DPP;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_CFG_G_DPP Message");
	        	myText += "Sending MSG_CFG_G_DPP Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private class getUARTSetupThread implements Runnable{
		
		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			Bundle b = new Bundle();
			msg.what = MessageTypes.MSG_CFG_G_UART;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_CFG_G_UART Message");
	        	myText += "Sending MSG_CFG_G_UART Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private class getSPISetupThread implements Runnable{

		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			Bundle b = new Bundle();
			msg.what = MessageTypes.MSG_CFG_G_SPI;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_CFG_G_SPI Message");
	        	myText += "Sending MSG_CFG_G_SPI Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private class getDIOSetupThread implements Runnable{
		
		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			Bundle b = new Bundle();
			msg.what = MessageTypes.MSG_CFG_G_IO;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_CFG_G_IO Message");
	        	myText += "Sending MSG_CFG_G_IO Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
			
		}
	}
	
	private class readDIOThread implements Runnable{

		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			Bundle b = new Bundle();
			msg.what = MessageTypes.MSG_READ_DIO;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_READ_DIO Message");
	        	myText += "Sending MSG_READ_DIO Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private class resetSettingsThread implements Runnable{

		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			Bundle b = new Bundle();
			msg.what = MessageTypes.MSG_ADMIN_RESET;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_ADMIN_RESET Message");
	        	myText += "Sending MSG_ADMIN_RESET Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private class commitSettingsThread implements Runnable{

		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			Bundle b = new Bundle();
			msg.what = MessageTypes.MSG_ADMIN_COMMIT;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_ADMIN_COMMIT Message");
	        	myText += "Sending MSG_ADMIN_COMMIT Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private class registerADCClientThread implements Runnable{

		@Override
		public void run() {
			//create and send a message
			Message msg = new Message();
			Bundle b = new Bundle();
			b.putInt("peripheral", IOBoardSettings.IOBOARD_PERIPHERAL_ADC);
			msg.what = MessageTypes.MSG_REGISTER_PERIPHERAL;
	        msg.replyTo = mMessenger;
	        msg.setData(b);
	        
	        try {
	        	Log.d(TAG, "Sending MSG_REGISTER_PERIPHERAL Message");
	        	myText += "Sending MSG_REGISTER_PERIPHERAL Message\n";
				mService.send(msg);
			} catch (RemoteException e1) {
				e1.printStackTrace();
			}
		}
	}

    
    private class ManagerThread extends Thread{
    	private boolean isRunning;
    	private boolean semaphore;
    	private ArrayList<Runnable> threadQueue;
    	private final long sampleDelay = 1000;
    	
    	public ManagerThread(){
    		threadQueue = new ArrayList<Runnable>();
			semaphore = false;
    	}

		/* (non-Javadoc)
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			super.run();
			
			long lastTime = Calendar.getInstance().getTimeInMillis();
			
			while(mService == null); //wait for connection to service to complete
			
			isRunning = true;
			
			while(isRunning){
				if(semaphore == false && threadQueue.isEmpty() == false){
					setThreadLock();
					new Thread(threadQueue.remove(0)).start();
				}
				
				if(Calendar.getInstance().getTimeInMillis() - lastTime > sampleDelay && boardSettings.getAdcMode() == IOBoardSettings.ADC_MODE_DISCREET){
					lastTime = Calendar.getInstance().getTimeInMillis();
					queue(new getADCSampleThread());
				}
			}
		}

		public boolean isRunning(){
			return isRunning;
		}
		
		public void stopRunning(){
			isRunning = false;
		}
		
		public void clearThreadLock(){
			semaphore = false;
		}
		
		public void setThreadLock(){
			semaphore = true;
		}
		
		public void queue(Runnable r){
			threadQueue.add(r);
		}
    	
    }

}
